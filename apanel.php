<!DOCTYPE html>

<html>
    <?php
        session_start();
        if ($_SESSION['privileges'] < 2)
        {
            header('Location: index.php');
        }
    ?>
	<head>
	<?php
		include "INCLUDES/head.php";
	?>
	</head>

	<body>

	<?php
		include "INCLUDES/header.php";
	?>

	<div id="content">
		<table id="posts" width="50%;">
			<tr><th>Title</th><th>Views</th><th>Responses</th><th>Date Created</th><th>Last Post</th><th>Tools</th></tr>
			<?php
				if (isset($_GET['cat']))
				{
					$servername = "127.0.0.1:3388";
					$username = "root";
					$password = "";
					$dbname = "xezforum";
					$conn = new mysqli($servername, $username, $password, $dbname);
					$sql = "SELECT * FROM postlist WHERE category='".$_GET['cat']."' ORDER BY id ASC;";
					$result = $conn->query($sql);
					if ($result->num_rows > 0)
					{
					    while($row = $result->fetch_assoc()) {
					        echo "<tr><td><a href=\"post.php?id=".$row['id']."\">".$row['title']."</a></td><td>".$row['views']."</td><td>".$row['responses']."</td><td>".$row['timestamp']."</td><td>.</td><td>Edit  Delete</td></tr>";
					    }
					}
					else
					{
						echo "No posts!";
					}
				}
				else
				{
					$servername = "127.0.0.1:3388";
					$username = "root";
					$password = "";
					$dbname = "xezforum";
					$conn = new mysqli($servername, $username, $password, $dbname);
					$sql = "SELECT * FROM postlist WHERE category='index';";
					$result = $conn->query($sql);
					if ($result->num_rows > 0)
					{
					    while($row = $result->fetch_assoc()) {
					        echo "<tr><td><a href=\"post.php?id=".$row['id']."\">".$row['title']."</a></td><td>".$row['views']."</td><td>".$row['responses']."</td><td>".$row['timestamp']."</td><td>.</td><td><a href=\"apanel.php?edit=".$row['id']."\">Edit</a>  <a href=\"apanel.php?delete=".$row['id']."\">Delete</a></td></tr>";
					    }
					}
					else
					{
						echo "No posts!";
					}
				}
				if ($_SESSION['username'] !== NULL)
				{
					echo "<form action=\"newpost.php\"><input type=\"submit\" value=\"New Post\" /><input type=\"hidden\" name=\"username\" value=\"".$_SESSION['username']."\" disabled /></form>";
				}
                if (isset($_GET['edit']))
                {
                    //echo "<br><br><br><br><form action=\"edit.php\"> <input type=\"textbox\" value=\"Title\" name=\"title\" /><br><br><textarea name=\"contents\">Content</textarea><br><input type=\"submit\" value=\"Edit\" /><br><br> </form>";
                    echo "<br>Feature not yet implemented.";
                }
                if (isset($_GET['delete']))
                {
					$servername = "127.0.0.1:3388";
					$username = "root";
					$password = "";
					$dbname = "xezforum";
					$conn = new mysqli($servername, $username, $password, $dbname);
					$sql = "DELETE FROM postlist WHERE id='".$_GET['delete']."';";
					$conn->query($sql);
                    header('Location: apanel.php');
                }
			?>
		</table>
	</div>
	<script type="text/javascript">
	   document.cookie = "sessionida=; expires Thu, 01 Jan 1970 00:00:00 UTC";
	   document.cookie = "sessionidb=; expires Thu, 01 Jan 1970 00:00:00 UTC";
	</script>

	</body>

</html>
