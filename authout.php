<?php
	session_start();
	// Unset all of the session variables
	unset($_SESSION['username']);
	unset($_SESSION['userid']);
	unset($_SESSION['privileges']);
	header("Location: index.php");
?>
